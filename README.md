# Radio Resilience Competition

## Getting Started

This guide covers setup and common usage of the Radio Resilience Competition local simulator environment.

This guide assumes you are using a computer running Ubuntu 20.04.

### [Prerequisites](#prerequisites)
### [Setup the Simulator](#setup-the-simulator)
### [Launch a Baudline IQ Monitor on the Host](#launch-a-baudline-iq-monitor-on-the-host)
### [Run a Match](#run-a-match)
### [Tweak the Competitor Radio](#tweak-the-competitor-radio)
### [Interactive Simulator](#interactive-simulator)
### [Power Limits](#power-limits)


## Prerequisites

- docker
- docker-compose
- GNU Radio (optional)
- baudline and gr-baz (optional)

## Setup the Simulator

To setup the simulator, clone the repository, and run the build script.

The build script uses `docker-compose` to build the competitor and infrastructure containres used by the simulator.

```
$ git clone https://gitlab.com/radio-resilience/simulator
$ cd simulator/sim/env
$ ./build.sh
```

The first run of the build script will take some time while it builds the base container image. Subsequent builds are much faster.

## Launch a Baudline IQ Monitor on the Host

For convenience, you can optionally run a baudline IQ monitor on the host. This provides an IQ visualization for any matches run on that computer.

```
$ cd simulator/grc
$ grcc host-baudline.grc
$ ./hostbaudline.py

```

## Run a Match

To run a match, you invoke `run-headless.sh`, which builds and starts the containers, runs a match, spins down the containers, and prints the score to the console.

If the baudline IQ monitor is running, it will display the spectrum while the match is running.

```
$ cd simulator/sim/env
$ ./run-headless.sh
...
Score: {match score}
```

## Tweak the Competitor Radio

The example competitor radio is a simple packet radio based on `liquid-dsp`, which you can tweak using GNU Radio Companion.

Run the following command in the root of the simulator repository:

```
$ gnuradio-companion sim/env/img/competitor/blue/blue1-base.grc
```

In GNU Radio Companion, the `Flexframe TX` block includes properties you can modify to change the modulation, inner FEC, and outer FEC.

Try modifying the default configuration, changing `FEC (Outer)` from `NONE` to `HAMMING74`. 

Now you can run another match and see if you observe an improved score.

## Interactive Simulator

The simulator can be run in interactive mode, where a GNU Radio Companion instance is launched for each node (except for the traffic marshal).

You can start the `muxer`, `red1`, `blue1`, and `blue2` radio flowgraphs manually in order to exercise the simulator outside of the match runner.

In order to run the competitor radios without the red radio, you would start the `muxer`, `blue1`, and `blue2`, for example.

To start the simulator in interactive mode, invoke `run-interactive.sh`.

```
$ cd simulator/sim/env
$ ./run-interactive.sh
```

The containers will spin up, and launch GNU Radio Companion instances. When you are done, you can trigger a graceful shutdown of the contianers by issuing `Ctrl+C` in the terminal where you invoked `run-interactive.sh`.

## Power Limits

The competitor and red radios are limited to a maximum transmit energy, computed as the sum of bins in a 1024-bin PSD. 

(For reference, the corresponding logic lives in the `psd_logger` block in `gr-rircsim`.)

After a match has completed, the maximum measured PSD values can be viewed as follows.

```
cat /tmp/blue1.psd-log | grep -vP '^-100\..+' | sort | head -n 1
cat /tmp/blue2.psd-log | grep -vP '^-100\..+' | sort | head -n 1
cat /tmp/red1.psd-log | grep -vP '^-100\..+' | sort | head -n 1
```