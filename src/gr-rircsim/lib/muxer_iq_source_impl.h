/* -*- c++ -*- */
/*
 * Copyright 2020 gr-rircsim author.
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this software; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street,
 * Boston, MA 02110-1301, USA.
 */

#ifndef INCLUDED_RIRCSIM_MUXER_IQ_SOURCE_IMPL_H
#define INCLUDED_RIRCSIM_MUXER_IQ_SOURCE_IMPL_H

#include <rircsim/muxer_iq_source.h>
#include <mutex>
#include <boost/bind.hpp>
#include <boost/asio.hpp>
#include <boost/thread.hpp>

#include "muxer_config.h"

using boost::asio::ip::tcp;

namespace gr {
  namespace rircsim {

    enum node_source_state_t
    {
      NODE_SOURCE_START,
      NODE_SOURCE_CONNECTED,
    };

    class muxer_iq_source_impl : public muxer_iq_source
    {
     private:

      std::string host;
      uint16_t port;
      node_source_state_t state;

      size_t sample_offset;
      muxer_iq_chunk_t m_iq_chunks[2];
      int m_input_chunk;
      int m_output_chunk;
      
      std::mutex m_mutex;

      boost::asio::io_service io_service;
      boost::shared_ptr<tcp::socket> socket;
      boost::thread m_thread;

      volatile bool m_write_pending;

      void client_loop();

      volatile bool m_cancel;

     public:
      muxer_iq_source_impl(std::string host, uint16_t port);
      ~muxer_iq_source_impl();

      bool stop();

      // Where all the action really happens
      int work(
              int noutput_items,
              gr_vector_const_void_star &input_items,
              gr_vector_void_star &output_items
      );
    };

  } // namespace rircsim
} // namespace gr

#endif /* INCLUDED_RIRCSIM_MUXER_IQ_SOURCE_IMPL_H */

